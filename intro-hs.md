# Curriculum for HS Intro-Class (6 weeks)

This is curriculum for a six-week introduction to coding for full-stack web development course that will be taught in 
partnership with the Community Connected Academy program at Patrick Henry High School in Minneapolis, Minnesota, USA.

At present, the program plans for intern interaction three times per week for one hour and thirty minutes each time. 
Students will have an opportunity to stay after the session is complete. Following this schedule, after Week 1, sessions will have two parts. 

### Part 1: Writing Scrapers
Each student will be able to pick a city or county for which to write a scraper. When that scraper is done, they will choose another city or county. 
A developer who works with the code will be available virtually to support the students, as will two volunteer classroom attendents.
Volunteers will be sourced through local companies and so be able to provide requisite credentials.

### Part 2: Weekly Focus
Each week we will focus on a new part of the web development stack (database, server, client, and deployment). We will use
the MnActivist platform to delve deeper into how the application is using the data interns are gathering.

## Week 1 Intro
### Day 1: Intro to Coding
- Crouton
- GitHub Acct
- Get Programs (Sublime3, Chromium/FireFox, PostgreSQL, etc)

### Day 2: WebPage Basics
- HTML, CSS, JS
- Write our first Scraper

### Day 3: Python Scrapers
- Write Complex scrapers
- Introduce Selenium

## Week 2 SQL / Database
### Day 1


### Day 2


### Day 3


## Week 3 Python/Django / Server
### Day 1
### Day 2
### Day 3

## Week 4 Vue / Client
### Day 1
### Day 2
### Day 3

## Week 5 Servers / Deployment
### Day 1
### Day 2
### Day 3

## Week 6 Wrap-Up
### Day 1
### Day 2
### Day 3
